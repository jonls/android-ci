#
# GitLab CI: Android
#
# Based on https://gitlab.com/showcheap/android-ci/blob/master/Dockerfile
#
FROM debian:jessie
MAINTAINER Jon Lund Steffensen <jonlst@gmail.com>

ENV VERSION_SDK_TOOLS "3859397"
ENV VERSION_BUILD_TOOLS "25.0.0 25.0.1 25.0.2 26.0.0"
ENV VERSION_TARGET_SDK "25"

ENV ANDROID_HOME "/android-sdk"

RUN echo "deb http://ftp.debian.org/debian jessie-backports main" > \
      /etc/apt/sources.list.d/jessie-backports.list

RUN apt-get -qq update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -qqy --no-install-recommends \
      curl \
      git \
      unzip \
      && \
    DEBIAN_FRONTEND=noninteractive apt-get install -qqy --no-install-recommends -t jessie-backports \
      openjdk-8-jdk-headless && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

ADD https://dl.google.com/android/repository/sdk-tools-linux-${VERSION_SDK_TOOLS}.zip /tools.zip
RUN unzip /tools.zip -d "${ANDROID_HOME}" && \
    rm -v /tools.zip

RUN echo y | "${ANDROID_HOME}"/tools/bin/sdkmanager "platforms;android-${VERSION_TARGET_SDK}" && \
    echo y | "${ANDROID_HOME}"/tools/bin/sdkmanager "platform-tools" && \
    for version in $VERSION_BUILD_TOOLS; do echo y | "${ANDROID_HOME}"/tools/bin/sdkmanager "build-tools;${version}"; done && \
    echo y | "${ANDROID_HOME}"/tools/bin/sdkmanager "extras;android;m2repository" && \
    echo y | "${ANDROID_HOME}"/tools/bin/sdkmanager "extras;google;google_play_services" && \
    echo y | "${ANDROID_HOME}"/tools/bin/sdkmanager "extras;google;m2repository"
